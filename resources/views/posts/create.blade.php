@extends('layouts.app') 
@section('content') 
<div class="container">
  <div id="submitDiv" class="mb-5">
    <div class="row justify-content-center">
      <div class="col-md-6 col-lg-5">
        <div class="card shadow-lg">
          <div class="card-header"><h5 class="mb-0"><strong>Write a Message</strong></h5></div>
          <div class="card-body">
            <form method="POST" action="/posts"> @csrf <div class="form-group" >
                <label for="title">To:</label>
                <input type="text" class="form-control" required maxlength="30" id="title" name="title" placeholder="Example: John">
              </div>
              <div class="form-group">
                <label for="content">Message:</label>
                <textarea name="content" class="form-control" required maxlength="100" id="content" placeholder="Type here your message you want to say"></textarea>
              </div>
              <div class="form-group text-center">
                <label for="theme">Theme</label>
                <br />

                <input type="radio" name="theme" id="peach" value="#cf5c60" checked required/>
                <label for="peach" class="labelRadio"><span style="background-color: #cf5c60;"></span></label>

                <input type="radio" name="theme" id="pink" value="#d96383"/>
                <label for="pink" class="labelRadio"><span style="background-color: #d96383;"></span></label>

                <input type="radio" name="theme" id="indigo" value="#717ecd"/>
                <label for="indigo" class="labelRadio"><span style="background-color: #717ecd;"></span></label>

                <input type="radio" name="theme" id="cyan" value="#4eb1cb"/>
                <label for="cyan" class="labelRadio"><span style="background-color: #4eb1cb;"></span></label>
                
                <input type="radio" name="theme" id="lime" value="#4ab471" />
                <label for="lime" class="labelRadio"><span style="background-color: #4ab471;"></span></label>

                <input type="radio" name="theme" id="orange" value="#f3ae4e"/>
                <label for="orange" class="labelRadio"><span style="background-color: #f3ae4e;"></span></label>

              </div>
              <div class="form-group text-center mx-1 mx-md-3">
                <label for="feeling">How are you feeling?</label>
                <br />
                <div class="btn-group" data-toggle="buttons">
                  <div class="row">
                  <div class="col-6 block p-0">
                   <label class="btn btn-light active block text-left">
                     <input checked required type="radio" name="feeling" value="happy"/>
                      <img class="mr-md-1" src="{{ asset('./images/owl-emoji/owl-emoji-happy.png') }}" height="40" width="40" alt="owl smile happy emoji emoticons feelings emotions sets">
                      happy
                   </label>
                    </div> 
                     <div class="col-6 block p-0">
                   <label class="btn btn-light block text-left">
                     <input type="radio" name="feeling" value="sad"/>
                      <img class="mr-md-1" src="{{ asset('./images/owl-emoji/owl-emoji-sad.png') }}"  height="40" width="40" alt="owl sad emoji emoticons feelings emotions sets">
                      sad
                   </label>
                    </div> 
                     <div class="col-6 block p-0">
                   <label class="btn btn-light block text-left">
                     <input type="radio" name="feeling" value="in love"/>
                      <img class="mr-md-1" src="{{ asset('./images/owl-emoji/owl-emoji-love.png') }}"  height="40" width="40" alt="owl heart love emoji emoticons feelings emotions sets">
                      in love
                   </label>
                    </div> 
                    <div class="col-6 block p-0">
                      <label class="btn btn-light block text-left">
                     <input type="radio" name="feeling" value="annoyed"/>
                      <img class="mr-md-1" src="{{ asset('./images/owl-emoji/owl-emoji-annoyed.png') }}"  height="40" width="40" alt="owl annoyed emoji emoticons feelings emotions sets">
                      annoyed
                   </label>
                    </div>
                    <div class="col-6 block p-0">
                   <label class="btn btn-light block text-left">
                     <input type="radio" name="feeling" value="angry"/>
                      <img class="mr-md-1" src="{{ asset('./images/owl-emoji/owl-emoji-angry.png') }}"  height="40" width="40" alt="owl angry emoji emoticons feelings emotions sets">
                      angry                   
                    </label>
                    </div> 
                     <div class="col-6 block p-0">
                   <label class="btn btn-light block text-left">
                     <input type="radio" name="feeling" value="shy"/>
                      <img class="mr-md-1" src="{{ asset('./images/owl-emoji/owl-emoji-shy.png') }}"  height="40" width="40" alt="owl shy emoji emoticons feelings emotions sets">
                      shy
                      </label>
                    </div> 
                    <div class="col-6 block p-0">
                      <label class="btn btn-light block text-left">
                     <input type="radio" name="feeling" value="drained"/>
                      <img class="mr-md-1" src="{{ asset('./images/owl-emoji/owl-emoji-drained.png') }}"  height="40" width="40" alt="owl drained emoji emoticons feelings emotions sets">
                      drained
                   </label>
                    </div>
                     <div class="col-6 block p-0">
                   <label class="btn btn-light block text-left">
                     <input type="radio" name="feeling" value="crazy"/>
                      <img class="mr-md-1" src="{{ asset('./images/owl-emoji/owl-emoji-crazy.png') }}"  height="40" width="40" alt="owl crazy emoji emoticons feelings emotions sets">
                      crazy
                   </label>
                    </div> 
                     <div class="col-6 block p-0">
                   <label class="btn btn-light block text-left">
                     <input type="radio" name="feeling" value="calm"/>
                      <img class="mr-md-1" src="{{ asset('./images/owl-emoji/owl-emoji-calm.png') }}"  height="40" width="40" alt="owl calm emoji emoticons feelings emotions sets">
                      calm
                   </label>
                    </div> 
                    <div class="col-6 block p-0">
                      <label class="btn btn-light block text-left">
                     <input type="radio" name="feeling" value="down"/>
                      <img class="mr-md-1" src="{{ asset('./images/owl-emoji/owl-emoji-down.png') }}"  height="40" width="40" alt="owl down emoji emoticons feelings emotions sets">
                      down
                   </label>
                    </div>
                  </div> 
                </div>
              
              </div>
              <div class="text-center mb-3">
                <button type="submit" class="btn btn-primary"><i class="fas fa-paper-plane"></i> Send</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div> 
@endsection
