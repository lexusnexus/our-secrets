@extends('layouts.app') 
@section('content') 
<div class="container">
  <h2 class="text-center definitionTitle">A freedom wall where we can say anything to our loved ones anonymously</h2>
<p class="text-center">
@if (count($posts) == 0) 
    <?php echo 'No Secret Message Found'; ?> 
  @elseif (count($posts) == 1)
  <?php echo count($posts).' Secret Message Found'; ?> 
  @else
  <?php echo count($posts).' Secret Messages Found'; ?> 
  @endif
</p>
  <div class="pb-3 px-3 pb-md-5 px-md-5 mt-3">
    <form class="form-inline nosubmit my-2 my-lg-0 justify-content-center search-form" action="/search" method="GET">
      <input class="form-control nosubmit mr-sm-2 col-11 col-md-6 search-field" type="text" name="search" required placeholder="Search for a name" aria-label="Search" maxlength="30">
    </form>
  </div>
  <div class="row justify-content-center"> 
    @if (count($posts) >= 1) 
    <?php $count = 0; ?> 
    @foreach ($posts as $post) 
    <?php  ?> 
    <?php if(stripos($_SERVER['REQUEST_URI'],'search?') == false ) 
    { if($count == 8) break;} ?>
    <div class="col-10 col-sm-6 col-md-4 col-lg-3 px-0 px-sm-2 px-md-3">
      <a href="/posts/{{ $post->id }}">
        <div class="card shadow-lg rounded cardPosts">
          <div class="card-header">
                        <table>
                <tr>
            <td rowspan="2" style="color: {{$post->theme}};"> <i style="font-size: 2.5em;" class="fas fa-user-circle changeFilter"></i></td>
            <td><h5 class="mb-0 pl-2">{{ $post->title }}</h5></td>
            </tr>
            <tr>
            <td> <p  class="pl-2" id="activeText"> Active {{ \Carbon\Carbon::parse($post->created_at)->diffForHumans() }}</p></td>
            </tr>
                 </table>
          </div>
          <div class="card-body messageCard" id="bodyDark">
            <div class="screen">
              <div class="conversation">
                <div class="messages messages--sent">
                  <div class="message changeFilter" style="background-color: {{$post->theme}};">{{$post->content}}</div>
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer text-muted">
            <div class="row" style="color: {{$post->theme}}; align-items: center;display: flex;">
              <div class="col-3 text-center">
                <i class="fas fa-chevron-circle-right changeFilter" id="backIcon"></i>
              </div>
              <div class="col-5 px-0">
                <input class="form-control fakeType" type="text" disabled placeholder="Aa" aria-label="Search" maxlength="30">
              </div>
              <div class="col-4 text-center">
                  @switch($post->feeling) 
                     @case("happy")
                      <img src="{{ asset('./images/owl-emoji/owl-emoji-happy.png') }}" height="40" width="40" alt="owl smile happy emoji emoticons feelings emotions sets">
                     @break
                  @case("sad")
                      <img src="{{ asset('./images/owl-emoji/owl-emoji-sad.png') }}"  height="40" width="40" alt="owl sad emoji emoticons feelings emotions sets">
                     @break
                   @case("in love")
                      <img src="{{ asset('./images/owl-emoji/owl-emoji-love.png') }}"  height="40" width="40" alt="owl heart love emoji emoticons feelings emotions sets">
                     @break
                      @case("annoyed")
                      <img src="{{ asset('./images/owl-emoji/owl-emoji-annoyed.png') }}"  height="40" width="40" alt="owl annoyed emoji emoticons feelings emotions sets">
                     @break
                      @case("angry")
                      <img src="{{ asset('./images/owl-emoji/owl-emoji-angry.png') }}"  height="40" width="40" alt="owl angry emoji emoticons feelings emotions sets">
                     @break
                      @case("shy")
                      <img src="{{ asset('./images/owl-emoji/owl-emoji-shy.png') }}"  height="40" width="40" alt="owl shy emoji emoticons feelings emotions sets">
                     @break
                      @case("drained")
                      <img src="{{ asset('./images/owl-emoji/owl-emoji-drained.png') }}"  height="40" width="40" alt="owl drained emoji emoticons feelings emotions sets">
                     @break
                      @case("crazy")
                      <img src="{{ asset('./images/owl-emoji/owl-emoji-crazy.png') }}"  height="40" width="40" alt="owl crazy emoji emoticons feelings emotions sets">
                     @break
                      @case("calm")
                      <img src="{{ asset('./images/owl-emoji/owl-emoji-calm.png') }}"  height="40" width="40" alt="owl calm emoji emoticons feelings emotions sets">
                     @break
                      @case("down")
                      <img src="{{ asset('./images/owl-emoji/owl-emoji-down.png') }}"  height="40" width="40" alt="owl down emoji emoticons feelings emotions sets">
                     @break
                        @default
                     <img src="{{ asset('./images/owl-emoji/owl-emoji-happy.png') }}" height="40" width="40" alt="owl smile happy emoji emoticons feelings emotions sets">
                  @endswitch
              </div>
            </div>
          </div>
      

    </div>
    </a>
  </div> <?php $count++; ?> @endforeach @else <div class="content noResults text-center p-3">
    <div class="title m-b-md">
      <h5 class="text-center"> <?php
                    if(isset($_GET['search']))                                        
                       echo 'No results found for '.'"'.$_GET['search'].'"';  
                    else
                       echo "No posts yet";?> </h5>
      <h5 class="text-center"> <?php
                    if(isset($_GET['search']))
                       echo "Try different name";?> </h5>
    </div>
    <div>
      <img src="{{ asset('./images/no-results.png') }}" height="250rem" alt="no results found icon">
    </div>
  </div> @endif
</div>
</div> 
@include('sweetalert::alert') 
@endsection