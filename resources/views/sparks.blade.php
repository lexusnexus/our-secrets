@extends('layouts.app')

@section('content')
<div class="container">
    <h1 class="mb-1 text-center">The <strong>Sparks Gallery</strong></h1>    
    <h4 class="mb-5 text-center">Some messages look connected to each other. Do they have the sparks?</h4>
     <div class="row justify-content-center"> 
   
    <?php $count = 0; ?> 
    @foreach ($posts as $post) 
    <?php if($count == 2) break; ?> 
       <div class="col-10 col-sm-6 col-md-6 col-lg-4 px-0 px-sm-2 px-md-3">
      <a href="/posts/{{ $post->id }}">
        <div class="card shadow-lg rounded cardPosts">
          <div class="card-header">
           <table>
                <tr>
            <td rowspan="2" style="color: {{$post->theme}};"> <i style="font-size: 2.5em;" class="fas fa-user-circle changeFilter"></i></td>
            <td><h5 class="mb-0 pl-2">{{ $post->title }}</h5></td>
            </tr>
            <tr>
            <td> <p  class="pl-2" id="activeText"> Active {{ \Carbon\Carbon::parse($post->created_at)->diffForHumans() }}</p></td>
            </tr>
                 </table>
          </div>
          <div class="card-body messageCard" id="bodyDark">
            <div class="screen">
              <div class="conversation">
                <div class="messages messages--sent">
                  <div class="message changeFilter" style="background-color: {{$post->theme}};">{{$post->content}}</div>
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer text-muted">
            <div class="row" style="color: {{$post->theme}}; align-items: center;display: flex;">
              <div class="col-3 text-center">
                <i class="fas fa-chevron-circle-right changeFilter" id="backIcon"></i>
              </div>
              <div class="col-5 px-0">
                <input class="form-control fakeType" type="text" disabled placeholder="Aa" aria-label="Search" maxlength="30">
              </div>
              <div class="col-4 text-center">
                @switch($post->feeling) 
                     @case("happy")
                      <img src="{{ asset('./images/owl-emoji/owl-emoji-happy.png') }}" height="40" width="40" alt="owl smile happy emoji emoticons feelings emotions sets">
                     @break
                  @case("sad")
                      <img src="{{ asset('./images/owl-emoji/owl-emoji-sad.png') }}"  height="40" width="40" alt="owl sad emoji emoticons feelings emotions sets">
                     @break
                   @case("in love")
                      <img src="{{ asset('./images/owl-emoji/owl-emoji-love.png') }}"  height="40" width="40" alt="owl heart love emoji emoticons feelings emotions sets">
                     @break
                      @case("annoyed")
                      <img src="{{ asset('./images/owl-emoji/owl-emoji-annoyed.png') }}"  height="40" width="40" alt="owl annoyed emoji emoticons feelings emotions sets">
                     @break
                      @case("angry")
                      <img src="{{ asset('./images/owl-emoji/owl-emoji-angry.png') }}"  height="40" width="40" alt="owl angry emoji emoticons feelings emotions sets">
                     @break
                      @case("shy")
                      <img src="{{ asset('./images/owl-emoji/owl-emoji-shy.png') }}"  height="40" width="40" alt="owl shy emoji emoticons feelings emotions sets">
                     @break
                      @case("drained")
                      <img src="{{ asset('./images/owl-emoji/owl-emoji-drained.png') }}"  height="40" width="40" alt="owl drained emoji emoticons feelings emotions sets">
                     @break
                      @case("crazy")
                      <img src="{{ asset('./images/owl-emoji/owl-emoji-crazy.png') }}"  height="40" width="40" alt="owl crazy emoji emoticons feelings emotions sets">
                     @break
                      @case("calm")
                      <img src="{{ asset('./images/owl-emoji/owl-emoji-calm.png') }}"  height="40" width="40" alt="owl calm emoji emoticons feelings emotions sets">
                     @break
                      @case("down")
                      <img  src="{{ asset('./images/owl-emoji/owl-emoji-down.png') }}"  height="40" width="40" alt="owl down emoji emoticons feelings emotions sets">
                     @break
                        @default
                     <img src="{{ asset('./images/owl-emoji/owl-emoji-happy.png') }}" height="40" width="40" alt="owl smile happy emoji emoticons feelings emotions sets">
                  @endswitch
              </div>
            </div>
          </div>
      </a>
    </div>
  </div> <?php $count++; ?> @endforeach
</div>
@endsection
