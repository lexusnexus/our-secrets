<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta property="og:title" content="Owl Secrets"/>
    <meta property="og:url" content="http://oursecrets.fun/"/>
    <meta property="og:image" content="{{ asset('./images/owl-secrets-meta.png') }}"/>
    <meta name="theme-color" content="#263748">
    <meta property="og:site_name" content="Owl Secrets"/>
    <!-- <meta property="fb:admins" content="USER_ID"/> -->
    <meta property="og:description"
          content="An online freedom wall where we can say anything to our loved ones anonymously."/>
    <link rel="icon" href="./images/owl-secrets-logo.png" />
    <link rel="icon" type="image/png" href="{{ asset('./images/owl-secrets-logo.png') }}">
    <title>{{-- {{ config('app.name', 'Our Secrets') }} --}}Owl Secrets</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    {{-- <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> --}}
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Varela+Round&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;700&family=Roboto:ital,wght@0,100;0,400;1,100;1,400&display=swap" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    {{-- <link rel="stylesheet" id="dark" href="{{ asset('css/light.css') }}" disabled> --}}
    <link rel="stylesheet" id="light" href="{{ asset('css/dark.css') }}" disabled>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
    <script src="https://unpkg.com/sweetalert@2.1.2/dist/sweetalert.min.js"></script>
    <script src="https://kit.fontawesome.com/7c5e9c611c.js" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script>
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <script>
    const darkmode = localStorage.getItem('darkmode');
    if (darkmode === '1') {
         document.getElementById("light").removeAttribute("disabled");
    }
    </script>
    <script type="text/javascript"
        src="https://cdn.jsdelivr.net/npm/@emailjs/browser@3/dist/email.min.js">
    </script>
    <script type="text/javascript">
   (function(){
      emailjs.init("JZOje06UtAZ3Wd4W-");
   })();
    </script>
    <style>
        .disclaimer { display: none; }
    </style>
</head>
<body class="d-flex flex-column min-vh-100">
<nav class="navbar navbar-light navbar-expand-md bg-white justify-content-center p-3 shadow-sm">
  <div class="container">
    
 
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsingNavbar3">
        <span class="navbar-toggler-icon"></span>
    </button>
     <a class="navbar-brand d-flex mr-auto pl-1 pl-md-0" href="{{ url('/') }}">
          <span class="navHead"> Owl </span>
          <span class="navSecrets"> Secrets </span>
        </a>
        <div class = "toggle-switch mr-1 d-md-none">
            <label id="switchToggle" class="mb-0 mt-1">
                <?php 
              $myItem = '';
              if(isset($_COOKIE['cookieName'])) {
                 $myItem = $_COOKIE['cookieName'];
                 if($myItem == "1") {
                  echo '<input type = "checkbox" id="checkbox" checked>';
                }
                else {
                echo '<input type = "checkbox" id="checkbox">';
              }
              } else {
                echo '<input type = "checkbox" id="checkbox">';
              }
              ?>
                <span class = "slider"></span>
            </label>
        </div>
    <div class="navbar-collapse collapse w-100" id="collapsingNavbar3">
        <ul class="navbar-nav w-100 justify-content-center">
            <li class="<?php if($_SERVER['REQUEST_URI'] === "/" ) { echo 'nav-item active'; } else { echo 'nav-item';} ?>" href="/about">
                <a class="nav-link" href="/">Home</a>
            </li>
            <li class="<?php if($_SERVER['REQUEST_URI'] === "/about" ) { echo 'nav-item active'; } else { echo 'nav-item';} ?>" href="/about">
                <a class="nav-link" href="/about">About</a>
            </li>
            <li class="<?php if($_SERVER['REQUEST_URI'] === "/sparks" ) { echo 'nav-item active'; } else { echo 'nav-item';} ?>" href="/about">
                <a class="nav-link" href="/sparks">Sparks</a>
            </li>
            <li class="<?php if($_SERVER['REQUEST_URI'] === "/contact" ) { echo 'nav-item active'; } else { echo 'nav-item';} ?>" href="/about">
                <a class="nav-link pb-3 pb-md-0" href="/contact">Contact</a>
            </li>
        </ul>
        <ul class="nav navbar-nav ml-auto w-100 justify-content-end">
            <li class="nav-item d-none d-md-block">
                <div class = "toggle-switch mr-3">
            <label id="switchToggle-web" class="mb-0 mt-1">
                <?php 
              $myItem = '';
              if(isset($_COOKIE['cookieName'])) {
                 $myItem = $_COOKIE['cookieName'];
                 if($myItem == "1") {
                  echo '<input type = "checkbox" id="checkbox-web" checked>';
                }
                else {
                echo '<input type = "checkbox" id="checkbox-web">';
              }
              } else {
                echo '<input type = "checkbox" id="checkbox-web">';
              }
              ?>
                <span class = "slider"></span>
            </label>
        </div>
            </li>
            <li class="<?php if($_SERVER['REQUEST_URI'] === "/posts/create" ) { echo 'nav-item d-none'; } else { echo 'nav-item';} ?>" id="sendBtn">
                <a role="button" class="btn btn-primary block" href="/posts/create"><i class="fas fa-plus"></i> Add Secret</a>
            </li>
        </ul>
    </div>
     </div>

</nav>  

    <button onclick="topFunction()" id="myBtn" title="Go to top">
      <i class="fas fa-chevron-up"></i>
    </button>
    <?php if($_SERVER['REQUEST_URI'] === "/" || stripos($_SERVER['REQUEST_URI'],'search?') == true ) { echo ' <div id="container2" class="my-5">
      <div id="text2"></div>
    </div>'; } ?>
    <div id="app">
      <main class="<?php if($_SERVER['REQUEST_URI'] === "/" || stripos($_SERVER['REQUEST_URI'],'search?') == true ) { echo 'pb-5'; } else { echo 'py-5';} ?>"> @yield('content') </main>
    </div>
    <div id="footer" class="mt-auto">
      <div class="container">
        <div class="row">
          <div class="col-12 text-center">
            <p class="mb-0 paragraphFooter"><i class="far fa-copyright"></i> Copyright 2022 | All rights reserved. </p>
            <div id="social-icons">
              <p class="mb-0 paragraphFooter">Created by <strong ><a class="editor" href = "mailto: melaithecoder@gmail.com">Melai The Coder</a></strong></p>
            </div>
          </div>
        </div>
      </div>
    </div> @include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"]) <script type="text/javascript">
      // List of sentences
      var _CONTENT = ["I still love you", "Sorry for everything", "Congratulations!", "Thank you, Ma"];
      // Current sentence being processed
      var _PART = 0;
      // Character number of the current sentence being processed 
      var _PART_INDEX = 0;
      // Holds the handle returned from setInterval
      var _INTERVAL_VAL;
      // Element that holds the text
      var _ELEMENT = document.querySelector("#text2");
      // Implements typing effect
      function Type() {
        var text = _CONTENT[_PART].substring(0, _PART_INDEX + 1);
        _ELEMENT.innerHTML = text;
        _PART_INDEX++;
        // If full sentence has been displayed then start to delete the sentence after some time
        if (text === _CONTENT[_PART]) {
          clearInterval(_INTERVAL_VAL);
          setTimeout(function() {
            _INTERVAL_VAL = setInterval(Delete, 50);
          }, 1000);
        }
      }
      // Implements deleting effect
      function Delete() {
        var text = _CONTENT[_PART].substring(0, _PART_INDEX - 1);
        _ELEMENT.innerHTML = text;
        _PART_INDEX--;
        // If sentence has been deleted then start to display the next sentence
        if (text === '') {
          clearInterval(_INTERVAL_VAL);
          // If last sentence then display the first one, else move to the next
          if (_PART == (_CONTENT.length - 1)) _PART = 0;
          else _PART++;
          _PART_INDEX = 0;
          // Start to display the next sentence after some time
          setTimeout(function() {
            _INTERVAL_VAL = setInterval(Type, 100);
          }, 200);
        }
      }
      // Start the typing effect on load
      _INTERVAL_VAL = setInterval(Type, 100);
      mybutton = document.getElementById("myBtn");
      // When the user scrolls down 20px from the top of the document, show the button
      window.onscroll = function() {
        scrollFunction()
      };

      function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
          mybutton.style.display = "block";
        } else {
          mybutton.style.display = "none";
        }
      }
      function topFunction() {
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
      }

    function setCookie(name,value,days) {
       var expires = "";
       if (days) {
           var date = new Date();
           date.setTime(date.getTime() + (days*24*60*60*1000));
           expires = "; expires=" + date.toUTCString();
       }
       document.cookie = name + "=" + (value || "")  + expires + "; path=/";
    }


      document.getElementById("checkbox").addEventListener("click", function() {
      var wasDarkMode = localStorage.getItem('darkmode') === '1';

      localStorage.setItem('darkmode', wasDarkMode ? '0' : '1');
      var myItem = localStorage.getItem('darkmode');
        setCookie('cookieName', myItem, 7);
       if (localStorage.getItem('darkmode') == '1') {
            document.getElementById("light").removeAttribute("disabled");
        } else {
                document.getElementById("light").setAttribute("disabled", "");
              }
        });
         document.getElementById("checkbox-web").addEventListener("click", function() {
          var wasDarkMode = localStorage.getItem('darkmode') === '1';

          localStorage.setItem('darkmode', wasDarkMode ? '0' : '1');
          var myItem = localStorage.getItem('darkmode');
            setCookie('cookieName', myItem, 7);
       if (localStorage.getItem('darkmode') == '1') {
            document.getElementById("light").removeAttribute("disabled");
        } else {
                document.getElementById("light").setAttribute("disabled", "");
              }
    });
    function onSubmit(token) {
         document.getElementById("demo-form").submit();
       }
    </script>
  </body>
</html>