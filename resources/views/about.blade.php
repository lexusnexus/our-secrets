@extends('layouts.app')

@section('content')
<div class="container">
    <div class="text-center d-md-none pb-3">
    <img src="{{ asset('./images/owl-big-blue-yellow.png') }}" class="aboutPic" alt="owl blue yellow emoticon emoji clipart">
    </div> 
        <h1 class="mb-1"><strong>Owl Secrets</strong></h1>    
        <h4 class="mb-3"><em>Be free. Let your heart speak.</em></h4>
        <div class="row justify-content-center">
        <div class="col-12 col-md-6 col-lg-8 order-2 order-md-1">
            <p class="my-3">
             <strong>Owl Secrets</strong> is an online freedom wall where you can say anything you want to your loved ones. The neat part? You are <strong>anonymous</strong>. So say whatever you want. Release your hidden feelings, explode and feel great! You can even select a color and an emoji to describe your emotions and feelings in writing the message. 
            </p>
            <p>
            No holds barred! <strong>Owl Secrets</strong> is about expressing and telling your feelings to someone without limitations. So what are you waiting for? Tell your lover, ex, best friend, friends, family, and even pets (because they deserve your love too) how much they mean to you. </p>
            <p>

            Don't worry because your secrets are safe with us. <strong>Your secrets are Owl Secrets.</strong>
            </p>
            <p>
            <strong>Owl Secrets</strong> started in 2022 and was made possible by <strong ><a href = "mailto: melaithecoder@gmail.com">Melai The Coder</a></strong>. All messages are submitted and stored digitally in an online storage. This online freedom wall aims to provide an emotional outlet for those in need.
            </p>

            <p>
                We used owl images from <a href="https://www.freevector.com/owl-emoticon-vector-set-18331">FreeVector.com</a>.
            </p>
        </div>
        <div class="col-12 col-md-6 col-lg-4 order-1 order-md-2 text-center d-none d-md-block">
            <img src="{{ asset('./images/owl-big-blue-yellow.png') }}" class="aboutPic" alt="owl blue yellow emoticon emoji clipart">
        </div>
    </div>
</div>
@endsection
