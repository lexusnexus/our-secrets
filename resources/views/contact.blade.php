@extends('layouts.app') 
@section('content') 
<div class="container">
    <div class="row justify-content-center text-center pt-3 pt-md-5">
      <div class="col-md-6 col-lg-6">
             <h1><strong>Get in touch</strong></h1>
             <h5>We care about your opinion.</h5>

             <p>Let us know if you have something interesting to say and share it with us.</p>
             <p><i class="fas fa-envelope"></i> melaithecoder@gmail.com</p>
              <a class="btn btn-primary" href = "mailto: melaithecoder@gmail.com">Message Us</a>
    </div>
  </div>
  @endsection
