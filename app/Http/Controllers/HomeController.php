<?php

namespace App\Http\Controllers;
use App\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function about()
    {
        return view('about');
    }
     public function sparks()
    {$dateS = new Carbon('first day of June 2022');
$dateE = new Carbon('last day of June 2022');
$result = Post::whereBetween('created_at', [$dateS->format('Y-m-d')." 00:00:00", $dateE->format('Y-m-d')." 23:59:59"])->get();
  // $posts_list = Post::all();
       return view('sparks')->with('posts',$result);
        // return view('sparks');
    }
     public function contact()
    {
        return view('contact');
    }
}
