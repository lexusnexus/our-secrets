<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Post;
use RealRashid\SweetAlert\Facades\Alert;
use Snipe\BanBuilder\CensorWords;
class PostController extends Controller
{
    public function index()
    {
        $posts_list = Post::all();
       return view('posts.index')->with('posts',$posts_list);
    }

    public function create()
    {
       return view('posts.create');
    }

    public function store(Request $req)
    {
        $censor = new CensorWords;
        $langs = array('en-us','en-uk','dictionary');
        $badwords = $censor->setDictionary($langs);
        $string = $censor->censorString($req->input('content'));
        $new_post = new Post;
        $new_post->title = $req->input('title');
        $new_post->content =$string['clean'];
        $new_post->theme = $req->input('theme');
        $new_post->feeling = $req->input('feeling');
        $saved = $new_post->save();
        if($saved){ 
           Alert::success('Awesome!','Your message has been sent successfully.');
        }
        return redirect('/');

    }
    public function switchModes()
      {
          if (session()->has('isDark')) {
              session()->put('isDark', !session('isDark'));
          }
          else {
              //provide an initial value of isDark
              session()->put('isDark', true);
          }
          return redirect('/');
    }

    public function search(Request $request){
        // Get the search value from the request
        $search = $request->input('search');

        // Search in the title and body columns from the posts table
        $posts = Post::query()
            ->where('title', 'LIKE', "{$search}")
            ->orWhere('content', 'LIKE', "%{$search}%")
            ->get();

        // Return the search view with the results compacted
        return view('posts.index')->with('posts',$posts);
    }
    
     public function show($post_id)
    {
        $existing_post = Post::find($post_id); // retrieve a single post object
        // dd($existing_post);
        return view('posts.show')->with('post', $existing_post);
    }



}
