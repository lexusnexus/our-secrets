<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use RealRashid\SweetAlert\Facades\Alert;
Route::get('/', function () {
    Alert::success('Info Title');
    return view('/');
});

Auth::routes();


Route::get('/', 'PostController@index')->name('post');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/sparks', 'HomeController@sparks')->name('sparks');
Route::get('/contact', 'HomeController@contact')->name('contact');
Route::get('/search', 'PostController@search')->name('search');
// Route::get('/switchModes', 'PostController@switchModes')->name('switchModes');

Route::get('/posts/create', 'PostController@create')->name('create');
Route::post('/posts', 'PostController@store')->name('store');
Route::get('/alert', 'PostController@alert')->name('alert');
Route::get('/posts/{post_id}', 'PostController@show')->name('show');

